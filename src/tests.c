#define _GNU_SOURCE
#include "tests.h"
#include "mem_internals.h"
#include "mem.h"

#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

static void* try_heap_init(size_t size) {
    debug("Heap initialization...\n");
    void *heap = heap_init(size);
    if (heap == NULL) {
        err("Unable to initialize heap!\n\n");
    }
    debug("Successful heap initialization!\n\n");
    return heap;
}

bool start_tests() {
    void *heap = try_heap_init(10000);
    if (test1(heap) && test2(heap) && test3(heap) && test4(heap) && test5(heap)) {
        return true;
    } else {
        return false;
    }
}

bool test1(void* heap) {
    debug(ANSI_COLOR_CYAN "Start test 1: \nTesting normal memory allocation..." ANSI_COLOR_RESET "\n\n");

    debug("Before allocation:\n");
    debug_heap(stdout, heap);

    debug("\nAfter allocation:\n");
    _malloc(1000);
    debug_heap(stdout, heap);

    if (heap) {
        debug("\n" ANSI_COLOR_GREEN "Test 1 complete" ANSI_COLOR_RESET "\n");
        debug("\n%s\n\n", "------------------------------------------");
        return true;
    }

    debug("\n" ANSI_COLOR_RED "Test 1 failed" ANSI_COLOR_RESET "\n");
    debug("\n%s\n\n", "------------------------------------------");
    return false;
}

bool test2(void* heap) {
    debug(ANSI_COLOR_CYAN "Start test 2: \nFree one block..." ANSI_COLOR_RESET "\n\n");

    void *block = _malloc(1000);

    debug("Before freeing a block:\n");
    debug_heap(stdout, heap);

    debug("\nAfter freeing a block:\n");
    _free(block);
    debug_heap(stdout, heap);

    if (block_get_header(block)->is_free) {
        debug("\n" ANSI_COLOR_GREEN "Test 2 complete" ANSI_COLOR_RESET "\n");
        debug("\n%s\n\n", "------------------------------------------");
        return true;
    }

    debug("\n" ANSI_COLOR_RED "Test 2 failed" ANSI_COLOR_RESET "\n");
    debug("\n%s\n\n", "------------------------------------------");
    return false;
}

bool test3(void* heap) {
    debug(ANSI_COLOR_CYAN "Start test 3: \nFree several blocks..." ANSI_COLOR_RESET "\n\n");

    void *block1 = _malloc(1000);
    void *block2 = _malloc(1000);

    debug("Before freeing the first block:\n");
    debug_heap(stdout, heap);

    debug("\nAfter freeing the second block:\n");
    _free(block2);
    debug_heap(stdout, heap);

    debug("\nAfter freeing the first block:\n");
    _free(block1);
    debug_heap(stdout, heap);

    if (block_get_header(block1)->is_free && block_get_header(block2)->is_free) {
        debug("\n" ANSI_COLOR_GREEN "Test 3 complete" ANSI_COLOR_RESET "\n");
        debug("\n%s\n\n", "------------------------------------------");
        return true;
    }

    debug("\n" ANSI_COLOR_RED "Test 3 failed" ANSI_COLOR_RESET "\n");
    debug("\n%s\n\n", "------------------------------------------");
    return false;
}

bool test4(void* heap) {
    debug(ANSI_COLOR_CYAN "Start test 4: \nMemory ended..." ANSI_COLOR_RESET "\n\n");


    debug("Heap before allocating a large block:\n");
    void *block1 = _malloc(500);
    debug_heap(stdout, heap);

    debug("\nHeap after allocating a large block:\n");
    void *block2 = _malloc(20000);
    debug_heap(stdout, heap);

    if (!block_get_header(block1)->is_free && !block_get_header(block2)->is_free) {
        debug("\n" ANSI_COLOR_GREEN "Test 4 complete" ANSI_COLOR_RESET "\n");
        debug("\n%s\n\n", "------------------------------------------");
        return true;
    }

    debug("\n" ANSI_COLOR_RED "Test 4 failed" ANSI_COLOR_RESET "\n");
    debug("\n%s\n\n", "------------------------------------------");

    return false;
}

static inline void mmap_region(size_t length, void *addr) {
    size_t count = (size_t) addr / getpagesize();
    size_t remains = (size_t) addr % getpagesize();

    uint8_t *total = (uint8_t *) (getpagesize() * (count + (remains > 1)));

    mmap(total, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, 0, 0);
}


static inline struct block_header* get_block_by_allocated_data(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}


bool test5(void* heap) {
    debug(ANSI_COLOR_CYAN "Start test 5: \nMemory ended and shifted..." ANSI_COLOR_RESET "\n\n");


    debug("Original heap:\n");
    debug_heap(stdout, heap);

    struct block_header *addr = heap;
    while (addr->next != NULL)
        addr = addr->next;


    mmap_region(200000, addr);

    void *allocated = _malloc(100000);

    debug("\nResult heap:\n");

    if (addr == get_block_by_allocated_data(allocated)) {
        debug("\n" ANSI_COLOR_RED "Test 5 failed" ANSI_COLOR_RESET "\n");
        debug("\n%s\n\n", "------------------------------------------");
        return false;
    }
    
    
    debug_heap(stdout, heap);
    _free(allocated);

    
    debug("\n" ANSI_COLOR_GREEN "Test 5 complete" ANSI_COLOR_RESET "\n");
    debug("\n%s\n\n", "------------------------------------------");
    return true;
}
