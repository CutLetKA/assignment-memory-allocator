#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

#include "stdbool.h"
#include <stdio.h>
#include "util.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

bool start_tests();
bool test1();
bool test2();
bool test3();
bool test4();
bool test5();

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
